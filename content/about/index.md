+++
title = "About"
description = "About me."
+++

<img id="logo" class="transparent drop-shadow" src="profil.jpg" width="128px" alt="logo" />

# Rat Cornu

## About

I am a 4th year french student in the computer science department at the ENS Paris-Saclay.
I like, among other things, animes, mangas, NixOS, Rust and ~~fluvial geomorphology~~ self-hosting. And **RATS**, of course ([Skavens](https://warhammerfantasy.fandom.com/wiki/Skaven) to be more precise).

That's why you will find here random tutos or explainations about whatever I feel like at the moment.

## Works

On my dev free time, I mostly code in nix and in Rust.

Currently, I'm working on [`efs`](https://codeberg.org/RatCornu/efs), a Rust `no-std` library to manage UNIX filesystems.

## Contact

Currently, it's <time><span id="clock"><noscript>JavaScript required</noscript></span></time> <small>(UTC+2)</small> for me, so take that into consideration if I don't respond promptly.

Feel free to send me a message on any of these.

<small>From most preferred to the least ↓</small>

* [Matrix](https://matrix.to/#/@ratcornu:skaven.org)
* [Mail](mailto:ratcornu+social@skaven.org)
* [Mail](mailto:ratcornu@crans.org) (if the first one is broken)
* Discord: `ratcornu`

## Socials

List of places you can find me on:

* [Lemmy](https://ani.social/u/RatCornu)
* Maybe Mastodon one day

## Forges

* [Codeberg](https://codeberg.org/RatCornu)
* [GitHub](https://github.com/RatCornu)

## Remarks

My pseudo, RatCornu, is composed of two french words : rat and cornu (horned).

It's the french name of the [Horned Rat](https://warhammerfantasy.fandom.com/wiki/Horned_Rat), the god of the Skavens (which is coherent with the fact that each of my electronic devices is named after a Skaven hero, city, ...). In this context, Skavenblight is also the name of the capital of the Under-Empire, the country of the Skavens.
