+++
title = "À propos"
description = "À propos de moi."
+++

<img id="logo" class="transparent drop-shadow" src="profil.jpg" width="128px" alt="logo" />

# Rat Cornu

## À propos

Je suis élève de 4e année au département informatique de l'ENS Paris-Saclay.
Entre autre, j'aime les animés, les mangas, NixOS, Rust et ~~la géomorphologie fluviale~~ le *self-hosting*. Et les **RATS** bien évidemment (les [Skavens](https://www.bibliotheque-imperiale.com/index.php/Skaven) pour être plus précis).

C'est pour cette raison que vous pourrez trouver ici des tutos, des explications sur des sujets variés sur lequels j'ai envie de parler sur le moment.

## Projets

Sur mon temps libre, j'apprécie programmer en nix et en Rust.

En ce moment, je travaille sur [`efs`](https://codeberg.org/RatCornu/efs), une librarie Rust `no-std` permettant de gérer des systèmes de fichiers UNIX.

## Contact

N'hésitez pas à me contacter avec l'un des moyens suivants.

<small>Par ordre décroissant de préférence ↓</small>

* [Matrix](https://matrix.to/#/@ratcornu:skaven.org)
* [Mail](mailto:ratcornu+social@skaven.org)
* [Mail](mailto:ratcornu@crans.org) (si le premier est cassé)
* Discord: `ratcornu`

## Social

Liste des endroits où vous pouvez me trouver :

* [Lemmy](https://ani.social/u/RatCornu)
* Peut-être un jour sur Mastodon

## Forges

* [Codeberg](https://codeberg.org/RatCornu)
* [GitHub](https://github.com/RatCornu)

## Remarques

Mon pseudo, RatCornu, est le nom du [Rat Cornu](https://www.bibliotheque-imperiale.com/index.php/Rat_Cornu), le dieu des Skavens (ce qui est cohérent avec le fait que tous mes appareils électroniques a un nom d'un héros Skaven, d'une ville, ...). Dans ce contexte, Skarogne est le nom de la capitale de l'Empire Souterrain, le pays des Skavens.
