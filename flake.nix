{
  inputs = {
    flake-parts.url = "github:hercules-ci/flake-parts";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
  };

  outputs =
    inputs@{ self
    , flake-parts
    , nixpkgs
    , ...
    }: flake-parts.lib.mkFlake { inherit inputs; } {
      systems = [
        "x86_64-linux"
      ];


      perSystem = { pkgs, ... }: {
        devShells = {
          default = pkgs.mkShell {
            name = "zola";

            packages = with pkgs; [
              mdl
              zola
            ];
          };
        };
      };
    };
}

